//
//  battleship.h
//
//
//  Created by David Heinrich on 3/6/16.
//
//

#include <cstdio>
#include <cstdlib>
#include <iostream>

using namespace std;
char oneShips[5][5], twoShips[5][5], redShips[5][5], blueShips[5][5];
int s1 = 0, s2 = 0, s3 = 0, s4 = 0, s5 = 0, s6 = 0, s7 = 0, s8 = 0;
void board();
int placeBoat(int, int, char, int);
int beg();
int attack(char, int, int);
int input(char, int, int);
char check(char);
char shipHit(int, char);


//Initialize all the different boards
int beg(){
    int i, j;
    for(i = 0; i < 5; i++)
        for(j = 0; j < 5; j++){
            oneShips[i][j] = '~';
            twoShips[i][j] = '~';
            redShips[i][j] = '~';
            blueShips[i][j] = '~';
        }
    return 0;
}

//Display the playing boards (oneShips/TwoShips) but we keep where1/where2 hidden to hid ship positions
void board(){
    int i, j;
    system("clear");
    cout<<"\nLet's Play Some BattleShip!"<<endl;
    
    cout<< "Coordinates to position ship or attack are in form: row (space) col ...where the rows and cols both start at 0 and end at 4";
    
    cout<<"\nPlayer One's Ship Map: "<<endl<<endl;
    for (i = 0; i < 5; i++) {
        cout<<"\t";
        for (j = 0; j < 5; j++)
            cout<<" "<<oneShips[i][j]<<"   ";
        cout<<endl;
    }
    
    cout<<"\nPlayer Two's Ship Map : "<<endl<<endl;
    for (i = 0; i < 5; i++)
    {
        cout<<"\t";
        for (j = 0; j < 5; j++)
            cout<<" "<<twoShips[i][j]<<"   ";
        cout<<endl;
    }
}

/* Original attack function
 int input(char person, int n, int m){
 if(n >= 0 && n <= 4 && m >= 0 && n <= 4){
 if(person == '1'){
 if(where1[n][m] == '~'){
 where1[n][m] = 's';
 return 0;
 }
 else{
 cout<<endl<<"You already have a ship in this spot! "<<endl;
 cout<<"Press Enter to continue"<<endl;
 do{}
 while(cin.get() != '\n');
 return 1;
 }
 }
 else{
 if(where2[n][m] == '~'){
 where2[n][m] = 's';
 return 0;
 }
 else{
 cout<<endl<<"You already have a ship in this spot! "<<endl;
 cout<<"Press Enter to continue"<<endl;
 do{}
 while(cin.get() != '\n');
 return 1;
 }
 }
 
 }
 else{
 cout<<"\nPlease enter Coordinates between 0-4. "<<endl;
 cout<<"Press Enter to continue"<<endl;
 do{}
 while(cin.get() != '\n');
 return 1;
 }
 
 }
 */

//checks whether all the ships on a player's board have been hit completely
char check(char player) {
    int i, j, q, p, hits=0, hits2=0;
    
    if (player == '1'){
        for (i = 0; i < 5; i++) {
            for (j = 0; j < 5; j++) {
                if (redShips[i][j] == 'H') {
                    hits++;
                }
            }
        }
        if(hits > 5){
            return 'l';
        }
    }
    
    else{
        for (q = 0; q < 5; q++) {
            for (p = 0; p < 5; p++) {
                if (blueShips[q][p] == 'H') {
                    hits2++;
                }
            }
        }
        if(hits2 > 5){
            return 'm';
        }
    }
    return 'p';
}


char shipHit(int s, char player) {
    cout<<"checking ship hit"<<endl;
    cout<<s<<endl;
    if (s % 3 == 0) {
        cout<<"You sunk a battleship!"<<endl;
    }
    return check(player);
}

//attack funciton that displays a hit or miss depending on user choice
int attack(char person, int n, int m){
    if(n >= 0 && n <= 4 && m >= 0 && n <= 4){
        if(person == '1'){
            if(redShips[n][m] == '1'){
                cout<<"That's a HIT! "<<endl;
                redShips[n][m] = 'H';
                oneShips[n][m] = 'H';
                s1++;
                shipHit(s1, '1');
                return 0;
            }
            if(redShips[n][m] == '2'){
                cout<<"That's a HIT! "<<endl;
                redShips[n][m] = 'H';
                oneShips[n][m] = 'H';
                s2++;
                shipHit(s2, '1');
                return 0;
            }
            if(redShips[n][m] == '3'){
                cout<<"That's a HIT! "<<endl;
                redShips[n][m] = 'H';
                oneShips[n][m] = 'H';
                s3++;
                shipHit(s3, '1');
                return 0;
            }
            if(redShips[n][m] == '4'){
                cout<<"That's a HIT! "<<endl;
                redShips[n][m] = 'H';
                oneShips[n][m] = 'H';
                s4++;
                shipHit(s4, '1');
                return 0;
            }
            if(redShips[n][m] == 'H'){
                cout<<"You already hit them there and wasted a turn ;( "<<endl;
                return 0;
            }
            if(redShips[n][m] == 'm'){
                cout<<"You already Missed them there and wasted a turn ;( "<<endl;
                return 0;
            }
            else{
                cout<<"Missed em "<<endl;
                redShips[n][m] = 'm';
                oneShips[n][m] = 'm';
                return 0;
            }
        }
        else{
            if(blueShips[n][m] == '5'){
                cout<<"That's a HIT! "<<endl;
                blueShips[n][m] = 'H';
                twoShips[n][m] = 'H';
                s5++;
                shipHit(s5, '2');
                return 0;
            }
            if(blueShips[n][m] == '6'){
                cout<<"That's a HIT! "<<endl;
                blueShips[n][m] = 'H';
                twoShips[n][m] = 'H';
                s6++;
                shipHit(s6, '2');
                return 0;
            }
            if(blueShips[n][m] == '7'){
                cout<<"That's a HIT! "<<endl;
                blueShips[n][m] = 'H';
                twoShips[n][m] = 'H';
                s7++;
                shipHit(s7, '2');
                return 0;
            }
            if(blueShips[n][m] == '8'){
                cout<<"That's a HIT! "<<endl;
                blueShips[n][m] = 'H';
                twoShips[n][m] = 'H';
                s8++;
                shipHit(s8, '2');
                return 0;
            }
            if(blueShips[n][m] == 'H'){
                cout<<"You already hit them there and wasted a turn ;( "<<endl;
                return 0;
            }
            if(blueShips[n][m] == 'm'){
                cout<<"You already Missed them there and wasted a turn ;( "<<endl;
                return 0;
            }
            else{
                cout<<"Missed em "<<endl;
                blueShips[n][m] = 'm';
                twoShips[n][m] = 'm';
                return 0;
            }
        }
    }
    else{
        cout<<"\nPlease enter Coordinates between 0-4"<<endl;
        cout<<"Press Enter to continue"<<endl;
        do{}
        while(cin.get() != '\n');
        return 1;
    }
}

//place boat somewhere on your board
int placeBoat(int x, int y, char name, int dir){
    if(x >= 0 && x <= 4 && y >= 0 && y <= 4 && 0 < dir && dir < 5){
        if (name == '2'){
            switch(dir){
                case 1:
                    redShips[x][y] = '1';
                    redShips[x-1][y] = '1';
                    redShips[x-2][y] = '1';
                    return 0;
                case 2:
                    redShips[x][y] = '2';
                    redShips[x][y+1] = '2';
                    redShips[x][y+2] = '2';
                    return 0;
                case 3:
                    redShips[x][y] = '3';
                    redShips[x+1][y] = '3';
                    redShips[x+2][y] = '3';
                    return 0;
                case 4:
                    redShips[x][y] = '4';
                    redShips[x][y-1] = '4';
                    redShips[x][y-2] = '4';
                    return 0;
            }
        }
        else{
            switch(dir){
                case 1:
                    blueShips[x][y] = '5';
                    blueShips[x-1][y] = '5';
                    blueShips[x-2][y] = '5';
                    return 0;
                case 2:
                    blueShips[x][y] = '6';
                    blueShips[x][y+1] = '6';
                    blueShips[x][y+2] = '6';
                    return 0;
                case 3:
                    blueShips[x][y] = '7';
                    blueShips[x+1][y] = '7';
                    blueShips[x+2][y] = '7';
                    return 0;
                case 4:
                    blueShips[x][y] = '8';
                    blueShips[x][y-1] = '8';
                    blueShips[x][y-2] = '8';
                    return 0;
            }
        }
    }
    else{
        cout<<"\nPlease enter Coordinates between 0-4"<<endl;
        cout<<"Press Enter to continue"<<endl;
        do{}
        while(cin.get() != '\n');
        return 1;
    }
    return 1;
}



