/*
** Steven Parker
** 3-8-2016
*/

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <thread> //thread

#define BUFF_SIZE 512

class ClientSocket{
    struct sockaddr_in _serv_addr;
    int _sockfd, _port;
    char _read_buffer[BUFF_SIZE];
    char _write_buffer[BUFF_SIZE];
    std::string password;

public:
    ClientSocket(int port, const char *ip_addr);
    void readFromSocket();
    void writeToSocket();
    void run();
    void testThreadR();
    void testThreadW();
};


ClientSocket::ClientSocket(int port, const char *ip_addr) {
    _port = port;
    //zero out serv_addr, just in case.
    bzero((char *) &_serv_addr, sizeof(_serv_addr));
    //Using IPv4
    _serv_addr.sin_family = AF_INET;
    //Making port network order
    _serv_addr.sin_port = htons(port);
    //Writing IP address to the sin_addr field in the serv_addr struct
    inet_pton(AF_INET, ip_addr, &_serv_addr.sin_addr);

    if ((_sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        std::cerr << "Socket is broken\n";
        std::cerr << "You broke it!\n";
        std::cerr << "WHY WOULD YOU DO THAT YOU ANIMAL\n";
        std::cerr << "I'm quitting now. Jerk." << std::endl;
        exit(1);
    }
    if (connect(_sockfd, (struct sockaddr *) &_serv_addr, sizeof(_serv_addr)) < 0) {
        std::cerr << "I can't make meaningful connections\n";
        std::cerr << "Probably because my parents never said they were proud of me...\n";
        std::cerr << "I mean, uh, ur connection failed. Sorry." << std::endl;
        exit(2);
    }
}

void ClientSocket::readFromSocket() {
    for (;;) {
        int retval;
        bzero(_read_buffer, BUFF_SIZE);
        if ((retval = read(_sockfd, _read_buffer, BUFF_SIZE-1)) < 0) {
            std::cerr << "This socket was written in Latin...\n";
            std::cerr << "I don't speak Latin...\n";
            std::cerr << "So uh.. Yeah. I can't read this. Try again maybe?" << std::endl;
            exit(3);
        }
        printf("%s\n", _read_buffer);
    }
}

void ClientSocket::writeToSocket() {
    for (;;) {
        int retval;
        std::string input;
        std::cout << "Enter a message: " << std::endl;
        std::cin >> input;
        if (input.length() > BUFF_SIZE - 1) {
            // User's message was too long for the buffer
            std::cerr << "Message not sent\n";
            std::cerr << "If you want to write a novel, use a word processor";
            std::cerr << std::endl;
        } else {
            bzero(_write_buffer,BUFF_SIZE);
            fgets(_write_buffer, BUFF_SIZE-1, stdin);
            if ((retval = write(_sockfd, _write_buffer, strlen(_write_buffer))) < 0) {
                std::cerr << "So uh\n";
                std::cerr << "Writing is pretty hard\n";
                std::cerr << "I can't do it\n";
                std::cerr << "Sorry :(\n" << std::endl;
                exit(69);
            }
        }
    }
}

void ClientSocket::run() {
    /*
    std::thread tRead(&ClientSocket::readFromSocket, this);
    std::thread tWrite(&ClientSocket::writeToSocket, this);
    */
    std::thread tRead(&ClientSocket::testThreadR, this);
    std::thread tWrite(&ClientSocket::testThreadW, this);

    tRead.join();
    tWrite.join();
    close(_sockfd);
}


//Purely for testing
void ClientSocket::testThreadR() {
    for (;;)
        std::cout << "foo" << std::endl;
}
//Purely for testing
void ClientSocket::testThreadW() {
    for (;;)
        std::cout << "bar" << std::endl;
}

int main(int argc, char *argv[]) {
    int port;

    if (argc != 3) {
        std::cerr << "Ok. You goofed it.\n";
        std::cerr << "Gotta say the hostname and then the port\n";
        std::cerr << "If you don't do that, I don't do anything.\n";
        std::cerr << "so uh. Yeah. Fix that." << std::endl;
        exit(111);
    }

    port = atoi(argv[2]);

    ClientSocket myself = ClientSocket(port, argv[1]);
    myself.run();

    return(0);
}
