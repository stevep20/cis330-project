/*
** Steven Parker
** 3-8-2016
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#define BUFF_SIZE 512

//Error-handling function to save lines
void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int main(int argc, char *argv[])
{
    int sockfd, port, retval;
    struct sockaddr_in serv_addr;
    char buffer[BUFF_SIZE];

    if (argc != 3) {
        error("Must give program name, hostname, and port.");
    }
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        error("Socket Failure! Failed to create socket.");
    }
    //Make port number an int
    port = atoi(argv[2]);
    //zero out serv_addr, just in case.
    bzero((char *) &serv_addr, sizeof(serv_addr));
    //Using IPv4
    serv_addr.sin_family = AF_INET;
    //Making port network order
    serv_addr.sin_port = htons(port);
    //Writing IP address to the sin_addr field in the serv_addr struct
    inet_pton(AF_INET, argv[1], &serv_addr.sin_addr);
    if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        error("Connection failed!");
    }
    for (;;){
        printf("Enter a message: ");
        bzero(buffer,BUFF_SIZE);
        fgets(buffer, BUFF_SIZE-1, stdin);
        if ((retval = write(sockfd, buffer, strlen(buffer))) < 0) {
            error("Write failure! Could not write to socket");
        }
        bzero(buffer, BUFF_SIZE);
        if ((retval = read(sockfd, buffer, BUFF_SIZE-1)) < 0) {
            error("Read failure! Could not read from socket");
        }
        printf("%s\n", buffer);
    }
    close(sockfd);
    return 0;
}
