/*
** Steven Parker
** 3-8-2016
*/

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <thread> //thread
#include "ui.hpp"
#include "passwordEncryption.hpp"

#define BUFF_SIZE 512

class ClientSocket{
    struct sockaddr_in _serv_addr;
    int _sockfd, _port;
    char _read_buffer[BUFF_SIZE];
    char _write_buffer[BUFF_SIZE];
    ChatScreen _screen;
    std::string _password;
    std::string _username;

public:
    ClientSocket(int port, char *ip_addr);
    void readFromSocket();
    void writeToSocket();
    void run();
    void testThreadR();
    void testThreadW();
};


ClientSocket::ClientSocket(int port, char *ip_addr) {
    _screen = ChatScreen();
    _screen.writeToDisplay("Enter a username: ");
    _username = _screen.getUserInput();
    std::string welcome = "Welcome, " + _username;
    _screen.writeToDisplay(welcome);
    //Get encryption password
    _screen.writeToDisplay("What is the password for your session?: ");
    _password = _screen.getUserInput();
    _screen.clearDisplay();
    _screen.writeToDisplay("Welcome to the server! Type something and say hello");

    _port = port;
    //zero out serv_addr, just in case.
    bzero((char *) &_serv_addr, sizeof(_serv_addr));
    //Using IPv4
    _serv_addr.sin_family = AF_INET;
    //Making port network order
    _serv_addr.sin_port = htons(port);
    //Writing IP address to the sin_addr field in the serv_addr struct
    inet_pton(AF_INET, ip_addr, &_serv_addr.sin_addr);

    if ((_sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        std::cerr << "Socket is broken\n";
        std::cerr << "You broke it!\n";
        std::cerr << "WHY WOULD YOU DO THAT YOU ANIMAL\n";
        std::cerr << "I'm quitting now. Jerk." << std::endl;
        exit(1);
    }
    if (connect(_sockfd, (struct sockaddr *) &_serv_addr, sizeof(_serv_addr)) < 0) {
        std::cerr << "I can't make meaningful connections\n";
        std::cerr << "Probably because my parents never said they were proud of me...\n";
        std::cerr << "I mean, uh, ur connection failed. Sorry." << std::endl;
        exit(2);
    }
}

void ClientSocket::readFromSocket() {
    int retval;
    std::string crypted;
    for (;;) {
        bzero(_read_buffer, BUFF_SIZE);
        if ((retval = read(_sockfd, _read_buffer, BUFF_SIZE-1)) < 0) {
            std::cerr << "This socket was written in Latin...\n";
            std::cerr << "I don't speak Latin...\n";
            std::cerr << "So uh.. Yeah. I can't read this. Try again maybe?" << std::endl;
            exit(3);
        }
        //Decrypting and printing message
        crypted = vigenereEncrypt(std::string(_read_buffer), _password, true, false, false, false);
        _screen.writeToDisplay(crypted);
        bzero(_read_buffer, BUFF_SIZE);
    }
}

void ClientSocket::writeToSocket() {
    int retval;
    std::string input, encrypted, usr_input, battleship;

    for (;;) {
        usr_input = _screen.getUserInput();
        input = _username + ": " + usr_input;
        battleship = usr_input.substr(0, 10);
        //Encrypting user's message
        encrypted = vigenereEncrypt(input, _password, false, false, false, false);
        if (battleship == "battleship"){
            _screen.writeToDisplay("Battleship(TM) DLC not installed...");
            _screen.writeToDisplay("Please pay $3.49 to developers for Battleship(TM) functionality.");
            _screen.writeToDisplay("Hey, us devs gotta eat, too");
        }
        if (encrypted.length() > BUFF_SIZE - 1) {
            // User's message was too long for the buffer
            _screen.writeToDisplay("Message not sent");
            _screen.writeToDisplay("If you want to write a novel, use a word processor");
        } else {
            bzero(_write_buffer,BUFF_SIZE);
            //Sending encrypted message
            encrypted.copy(_write_buffer, encrypted.length(), 0);
            if ((retval = write(_sockfd, _write_buffer, strlen(_write_buffer))) < 0) {
                std::cerr << "So uh\n";
                std::cerr << "Writing is pretty hard\n";
                std::cerr << "I can't do it\n";
                std::cerr << "Sorry :(\n" << std::endl;
                exit(69);
            }
        }
        bzero(_write_buffer, BUFF_SIZE);
    }
}

void ClientSocket::run() {

    std::thread tRead(&ClientSocket::readFromSocket, this);
    std::thread tWrite(&ClientSocket::writeToSocket, this);
    /*
    std::thread tRead(&ClientSocket::testThreadR, this);
    std::thread tWrite(&ClientSocket::testThreadW, this);
    */
    tRead.join();
    tWrite.join();
    close(_sockfd);
}


//Purely for testing
void ClientSocket::testThreadR() {
    for (;;)
        std::cout << "foo" << std::endl;
}
//Purely for testing
void ClientSocket::testThreadW() {
    for (;;)
        std::cout << "bar" << std::endl;
}

int main(int argc, char *argv[]) {

    if (argc != 3) {
        std::cerr << "Ok. You goofed it.\n";
        std::cerr << "Gotta say the hostname and then the port\n";
        std::cerr << "If you don't do that, I don't do anything.\n";
        std::cerr << "so uh. Yeah. Fix that." << std::endl;
        exit(111);
    }

    int port;
    struct hostent *host = gethostbyname(argv[1]);
    struct in_addr *address = (in_addr *)host->h_addr;
    std::string temp_ip = inet_ntoa(*address);
    char ip_address[16] = {};
    temp_ip.copy(ip_address, temp_ip.length(), 0);

    port = atoi(argv[2]);

    ClientSocket myself = ClientSocket(port, ip_address);
    myself.run();

    return(0);
}
