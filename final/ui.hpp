#ifndef UI_HPP_
#define UI_HPP_

#include <ncurses.h>
#include <iostream>
#include <string>
#include <vector>
#include <string.h>

class ChatScreen {
    WINDOW *_display, *_entry;
    int _max_x, _max_y; // The size of the current terminal window

    public:
        ChatScreen();
        std::string getUserInput();
        void writeToDisplay(std::string message);
        void clearDisplay();
};

#endif // UI_HPP_
