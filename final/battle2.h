//
//  battle2.h
//
//
//David Heinrich
//
//


#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <string>


class Board{
    int whichShip = 1, s1 = 0, s2 = 0;
    /*one and two ships are shown maps while red and blue ships are the hidden
     and store where the boats are hidden*/
    char oneShips[5][5], redShips[5][5];
    Board();
    void placeboat(int, int, int);
    void beg();
    void attack(int, int);
    bool isLoss();
    bool isShipHit(int);
    std::vector<std::string> represent();
};


Board::Board(){}


void Board::beg(){
    //initialize the board
    int i, j;
    for(i = 0; i < 5; i++){
        for(j = 0; j < 5; j++){
            oneShips[i][j] = '~';
            redShips[i][j] = '~';
        }
    }
}


//display the ship map that the user will use to determine their attack based on previous attacks
std::vector<std::string> Board::represent(){
    std::vector<std::string> strings;
    for(int i = 0; i < 5; i++){
        strings.push_back(std::string(oneShips[i]));
    }
    return strings;
}


//function places a boat depending on coordinate and a direction
void Board::placeboat(int x, int y, int dir){
    if(x >= 0 && x <= 4 && y >= 0 && y <= 4 && 0 < dir && dir < 5){
        if(whichShip == 1){
            switch(dir){
                case 1: //direction: up
                    redShips[x][y] = '1';
                    redShips[x-1][y] = '1';
                    redShips[x-2][y] = '1';
                case 2: //direction:right
                    redShips[x][y] = '1';
                    redShips[x][y+1] = '1';
                    redShips[x][y+2] = '1';
                case 3: //direction:down
                    redShips[x][y] = '1';
                    redShips[x+1][y] = '1';
                    redShips[x+2][y] = '1';
                case 4: //direction:left
                    redShips[x][y] = '1';
                    redShips[x][y-1] = '1';
                    redShips[x][y-2] = '1';
            }
        }
        else{
            switch(dir){
                case 1:
                    redShips[x][y] = '2';
                    redShips[x-1][y] = '2';
                    redShips[x-2][y] = '2';
                case 2:
                    redShips[x][y] = '2';
                    redShips[x][y+1] = '2';
                    redShips[x][y+2] = '2';
                case 3:
                    redShips[x][y] = '2';
                    redShips[x+1][y] = '2';
                    redShips[x+2][y] = '2';
                case 4:
                    redShips[x][y] = '2';
                    redShips[x][y-1] = '2';
                    redShips[x][y-2] = '2';
            }
        }
        
    }
    whichShip++;
}


//attack method that takes a coordinate and will either return a hit or miss
void Board::attack(int n, int m){
    if(n >= 0 && n <= 4 && m >= 0 && n <= 4){
        if(redShips[n][m] == '1'){
            //cout<<"That's a HIT! "<<endl;
            redShips[n][m] = 'H';
            oneShips[n][m] = 'H';
            s1++;
            isShipHit(s1);
        }
        if(redShips[n][m] == '2'){
            //cout<<"That's a HIT! "<<endl;
            redShips[n][m] = 'H';
            oneShips[n][m] = 'H';
            s2++;
            isShipHit(s2);
        }
        if(redShips[n][m] == 'H'){
            //cout<<"You already hit them there and wasted a turn ;( "<<endl;
        }
        if(redShips[n][m] == 'm'){
            //cout<<"You already Missed them there and wasted a turn ;( "<<endl;
        }
        else{
            //cout<<"Missed em "<<endl;
            redShips[n][m] = 'm';
            oneShips[n][m] = 'm';
        }
    }
    
}


//returns whether or not the player has lost the game or not
bool Board::isLoss(){
    int i, j, hits=0;
    for (i = 0; i < 5; i++) {
        for (j = 0; j < 5; j++) {
            if (redShips[i][j] == 'H') {
                hits++;
            }
        }
    }
    if(hits > 5){
        return true;
    }
    return false;
}

//returns whether or not a players ship has been sunk
bool Board::isShipHit(int s){
    if (s % 3 == 0) {
        return true;
    }
    return false;
}
