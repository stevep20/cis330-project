//Portia Seater

#ifndef passwordEncryption_H_
#define passwordEncryption_H_

//Size of alphabet is constant
#define ALPHABET_SIZE 26

//Converts letters to corresponding numbers
int getLetterNum(char letter);

//Converts numbers to corresponding letters
char getCharFromNum(int num);

// Converts a string of numeric characters to the corresponding letters
std::string numericKeyToAlphabetical(std::string numericKey);

//Performs both vigenere encryoting and decrypting
std::string vigenereEncrypt(std::string text, std::string keytext, bool decrypt /*=false*/, bool encryptSpaces /*=false*/, bool allLowerCase/*=false*/, bool stripUnsupportedChars/*=false*/);

//Enters the session of the input password
std::string getPassword();

#endif /* passwordEncryption_H_ */
