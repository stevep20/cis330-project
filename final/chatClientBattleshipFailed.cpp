/*
** Steven Parker
** 3-8-2016
*/

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <thread> //thread
#include "ui.hpp"
#include "passwordEncryption.hpp"
#include "battle2.h"

#define BUFF_SIZE 512

class ClientSocket{
    struct sockaddr_in _serv_addr;
    int _sockfd, _port;
    char _read_buffer[BUFF_SIZE];
    char _write_buffer[BUFF_SIZE];
    ChatScreen _screen;
    Board _board;
    bool _isPlaying;
    bool _hasShips;
    std::string _opponent;
    std::string _password;
    std::string _username;

public:
    ClientSocket(int port, char *ip_addr);
    void readFromSocket();
    void writeToSocket();
    void run();
    void testThreadR();
    void testThreadW();
};


ClientSocket::ClientSocket(int port, char *ip_addr) {
    _isPlaying = false;
    _hasShips = false;
    _screen = ChatScreen();
    _screen.writeToDisplay("Enter a username: ");
    _username = _screen.getUserInput();
    std::string welcome = "Welcome, " + _username;
    _screen.writeToDisplay(welcome);
    //Get encryption password
    _screen.writeToDisplay("What is the password for your session?: ");
    _password = _screen.getUserInput();

    _port = port;
    //zero out serv_addr, just in case.
    bzero((char *) &_serv_addr, sizeof(_serv_addr));
    //Using IPv4
    _serv_addr.sin_family = AF_INET;
    //Making port network order
    _serv_addr.sin_port = htons(port);
    //Writing IP address to the sin_addr field in the serv_addr struct
    inet_pton(AF_INET, ip_addr, &_serv_addr.sin_addr);

    if ((_sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        std::cerr << "Socket is broken\n";
        std::cerr << "You broke it!\n";
        std::cerr << "WHY WOULD YOU DO THAT YOU ANIMAL\n";
        std::cerr << "I'm quitting now. Jerk." << std::endl;
        exit(1);
    }
    if (connect(_sockfd, (struct sockaddr *) &_serv_addr, sizeof(_serv_addr)) < 0) {
        std::cerr << "I can't make meaningful connections\n";
        std::cerr << "Probably because my parents never said they were proud of me...\n";
        std::cerr << "I mean, uh, ur connection failed. Sorry." << std::endl;
        exit(2);
    }
}

void ClientSocket::readFromSocket() {
    int retval;
    std::string crypted;
    std::string battleship;
    for (;;) {
        bzero(_read_buffer, BUFF_SIZE);
        if ((retval = read(_sockfd, _read_buffer, BUFF_SIZE-1)) < 0) {
            std::cerr << "This socket was written in Latin...\n";
            std::cerr << "I don't speak Latin...\n";
            std::cerr << "So uh.. Yeah. I can't read this. Try again maybe?" << std::endl;
            exit(3);
        }
        //Decrypting and printing message
        crypted = vigenereEncrypt(std::string(_read_buffer), _password, true, false, false, false);
        battleship = crypted.substr(crypted.find("battleship"), 10);
        if (battleship == "battleship"){
            if(!_isPlaying && _username == crypted.substr(crypted.find("battleship") + 12, std::string::npos)) {
                _isPlaying = true;
                _opponent = usr_input.substr(0, crypted.find(":"));
                _board = Board();
                _board.beg();
            }
            else if(_isPlaying && _hasShips) {
                //do stuff
            }
            continue;
        }
        _screen.writeToDisplay();
        bzero(_read_buffer, BUFF_SIZE);
    }
}

void ClientSocket::writeToSocket() {
    int retval, x_ship, y_ship, dir;
    std::string raw_ship;
    std::string input;
    std::string encrypted;
    std::string usr_input;
    std::string battleship;
    for (;;) {
        usr_input = _screen.getUserInput();
        input = _username + ": " + usr_input;
        battleship = usr_input.substr(0, 10)
        //Encrypting user's message
        encrypted = vigenereEncrypt(input, _password, false, false, false, false);
        if (battleship == "battleship"){
            if(!_isPlaying) {
                _isPlaying = true;
                _opponent = usr_input.substr(11, std::string::npos);
                _board = Board();
                _board.beg();
                _screen.writeToDisplay("Please enter the coordinates for the noses of both ships.");
                _screen.writeToDisplay("Entry is of the form: ship1_x ship1_y dir1 ship2_x ship2_y dir2");
                _screen.writeToDisplay("Where dir can be 1, 2, 3, or 4");
                _screen.writeToDisplay("1 means tail points north, 2 means tail points east,");
                _screen.writeToDisplay("3 means tail points south, 4 means tail points west.");
            }
            if(!_hasShips) {
                raw_ship = _screen.getUserInput();
                x_ship << raw_ship.substr(0, 1);
                y_ship << raw_ship.substr(2, 1);
                dir << raw_ship.substr(4, 1);
                _board.placeboat(x_ship, y_ship, dir);
                x_ship << raw_ship.substr(6, 1);
                y_ship << raw_ship.substr(8, 1);
                dir << raw_ship.substr(10, 1);
                _board.placeboat(x_ship, y_ship, dir);
            }
            else if(_isPlaying && _hasShips) {
                //do stuff
            }
        }
        if (encrypted.length() > BUFF_SIZE - 1) {
            // User's message was too long for the buffer
            std::cerr << "Message not sent\n";
            std::cerr << "If you want to write a novel, use a word processor";
            std::cerr << std::endl;
        } else {
            bzero(_write_buffer,BUFF_SIZE);
            //Sending encrypted message
            encrypted.copy(_write_buffer, encrypted.length(), 0);
            if ((retval = write(_sockfd, _write_buffer, strlen(_write_buffer))) < 0) {
                std::cerr << "So uh\n";
                std::cerr << "Writing is pretty hard\n";
                std::cerr << "I can't do it\n";
                std::cerr << "Sorry :(\n" << std::endl;
                exit(69);
            }
        }
        bzero(_write_buffer, BUFF_SIZE);
    }
}

void ClientSocket::run() {

    std::thread tRead(&ClientSocket::readFromSocket, this);
    std::thread tWrite(&ClientSocket::writeToSocket, this);
    /*
    std::thread tRead(&ClientSocket::testThreadR, this);
    std::thread tWrite(&ClientSocket::testThreadW, this);
    */
    tRead.join();
    tWrite.join();
    close(_sockfd);
}


//Purely for testing
void ClientSocket::testThreadR() {
    for (;;)
        std::cout << "foo" << std::endl;
}
//Purely for testing
void ClientSocket::testThreadW() {
    for (;;)
        std::cout << "bar" << std::endl;
}

int main(int argc, char *argv[]) {

    if (argc != 3) {
        std::cerr << "Ok. You goofed it.\n";
        std::cerr << "Gotta say the hostname and then the port\n";
        std::cerr << "If you don't do that, I don't do anything.\n";
        std::cerr << "so uh. Yeah. Fix that." << std::endl;
        exit(111);
    }

    int port;
    struct hostent *host = gethostbyname(argv[1]);
    struct in_addr *address = (in_addr *)host->h_addr;
    std::string temp_ip = inet_ntoa(*address);
    char ip_address[16] = {};
    temp_ip.copy(ip_address, temp_ip.length(), 0);

    port = atoi(argv[2]);

    ClientSocket myself = ClientSocket(port, ip_address);
    myself.run();

    return(0);
}
