#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>
#include <iostream>
#include <string>
#include <stdint.h>
#include <pthread.h>
#include <vector>

#define BUF_SIZE 512

class ServerSocket {
    int _serversock;
    std::vector<int> _clients;
    fd_set _socks;
    int _portnum;
    char _buffer[BUF_SIZE];
    
    public:
        ServerSocket(int portnum);
        void acceptClient();
        void payAttention();
        void handleIncomingMessage(int incomingClient);
};

ServerSocket::ServerSocket(int portnum) {
    _portnum = portnum;
    struct sockaddr_in server_addr;
    // Make everything zeros in the address struct and fd_set:
    bzero((char *) &server_addr, sizeof(server_addr));
    FD_ZERO(&_socks);
    // Initialize the server address:
    server_addr.sin_family = AF_INET;
    // htons converts a port number in host byte order
    // to a port number in network byte order
    server_addr.sin_port = htons(_portnum);
    // This is the IP of the server. INADDR_ANY is a symbolic constant
    // which will get this address for us.
    server_addr.sin_addr.s_addr = INADDR_ANY;
    
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0) {
        std::cerr << "wow something went super wrong there\n";
        std::cerr << "jeez this hasn't really ever happened before\n";
        std::cerr << "i dunno how to even handle this\n";
        std::cerr << "sorry";
        std::cerr << std::endl;
        exit(100);
    }
    
    // I don't wanna explain this one. Just check the man page for bind()
    if (bind(sock,(struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) {
        std::cerr << "uh\n";
        std::cerr << "there was an error binding the socket to the address ";
        std::cerr << "I guess\nso uh\nthat sucks";
        std::cerr << std::endl;
        exit(101);
    }
    
    // Now that we have bound sock to our address, we can listen for stuff.
    // 5 is the max number of connections that can be queued up while we're
    // processing one connection. Some web page said 5 was the max supported
    // by most systems so that's what we're going with I guess
    listen(sock, 5);
    
    // Add server socket to fd_set
    FD_SET(sock, &_socks);
    _serversock = sock;
    
}

void ServerSocket::acceptClient() {
    struct sockaddr_in client_addr;
    socklen_t client_len = sizeof(client_addr); // We have to give a library
    // function a pointer to this later, which I think is really dumb. But
    // c'est la vie.
    
    // Make everything zeros in the address struct:
    bzero((char *) &client_addr, sizeof(client_addr));
    
    int clientsock;
    clientsock = accept(_serversock,(struct sockaddr *) &client_addr,&client_len);
    // Kinda bothers me that we have to give &client_len as an argument.
    // Like, what?
    if (clientsock < 0) {
        std::cerr << "there are literally so many errors that can possibly";
        std::cerr << " happen in this program\ni can't even";
        exit(989);
    }
    
    FD_SET(clientsock, &_socks);
    _clients.push_back(clientsock);
}

void ServerSocket::payAttention() {
    int newMessageEvent;
    fd_set read_fd_set;
    for ( ;;) {
        read_fd_set = _socks;
        newMessageEvent = select(FD_SETSIZE, &read_fd_set, 0, 0, 0);
        if (newMessageEvent < 0) {
            std::cerr << "Selection error. It's probably your fault.";
            std::cerr << std::endl;
            exit(-12);
        }
        for (int i=0; i < FD_SETSIZE; ++i) {
            if (FD_ISSET(i, &read_fd_set)) {
                // We have a pending request on this socket
                if (i == _serversock) {
                    // New incoming connection
                    this->acceptClient();
                } else {
                    // A client sent a new message in
                    this->handleIncomingMessage(i);
                }
            }
        }

    }
    
}

void ServerSocket::handleIncomingMessage(int incomingClient) {
    bzero(_buffer, BUF_SIZE);
    
    // Read the pending message into the buffer:
    int result = read(incomingClient, _buffer, BUF_SIZE - 1);
    // result contains the number of characters read, or an error code
    if (result < 0) {
        std::cerr << "could not read from socket. sorry for fail";
        std::cerr << std::endl;
        exit(69); // giggity
    }
    
    // Echo the message to every client except the one that sent it:
    for (int otherclient : _clients) {
        if (true | (otherclient != incomingClient)) { //FIXME remove "true |"
            write(otherclient, _buffer, result);
        }
    }
}

int main(int argc, char *argv[]) {
    int portnum;
    
    if (argc != 2) {
        // User was supposed to give a port as an argument
        std::cerr << "u didn't do that right bruh\n";
        std::cerr << "u gotta specify a port bruh";
        std::cerr << std::endl;
        exit(111);
    }
    portnum = atoi(argv[1]);
    
    ServerSocket myself = ServerSocket(portnum);
    myself.payAttention();
    
    return(0);
    
    
    
}