//
//  battleship.h
//  
//
//  Created by David Heinrich on 3/6/16.
//
//

#include <cstdio>
#include <cstdlib>
#include <iostream>

using namespace std;
char oneShips[5][5], twoShips[5][5], where1[5][5], where2[5][5];
void board();
int placeBoat(int, int, char, int);
int beg();
int attack(char, int, int);
int input(char, int, int);
char check();
char shipHit(char, char);


//Initialize all the different boards
int beg(){
    int i, j;
    for(i = 0; i < 5; i++)
        for(j = 0; j < 5; j++){
            oneShips[i][j] = '~';
            twoShips[i][j] = '~';
            where1[i][j] = '~';
            where2[i][j] = '~';
        }
    return 0;
}

//Display the playing boards (oneShips/TwoShips) but we keep where1/where2 hidden to hid ship positions
void board(){
    int i, j;
    system("clear");
    cout<<"\n Let's Play Some BattleShip!\n"<<endl;
    
    cout<< "Coordinates to position ship or attack are in form: row (space) col ...where the rows and cols both start at 0 and end at 4";
    
    cout<<"\nPlayer One's Ship Map: "<<endl<<endl;
    for (i = 0; i < 5; i++) {
        cout<<"\t";
        for (j = 0; j < 5; j++)
            cout<<" "<<oneShips[i][j]<<"   ";
        cout<<endl;
    }
    cout<<"\nPlayer Two's Ship Map : "<<endl<<endl;
    for (i = 0; i < 5; i++)
    {
        cout<<"\t";
        for (j = 0; j < 5; j++)
            cout<<" "<<twoShips[i][j]<<"   ";
        cout<<endl;
    }
}

/* Original attack function 
int input(char person, int n, int m){
    if(n >= 0 && n <= 4 && m >= 0 && n <= 4){
        if(person == '1'){
            if(where1[n][m] == '~'){
                where1[n][m] = 's';
                return 0;
            }
            else{
                cout<<endl<<"You already have a ship in this spot! "<<endl;
                cout<<"Press Enter to continue"<<endl;
                do{}
                while(cin.get() != '\n');
                return 1;
            }
        }
        else{
            if(where2[n][m] == '~'){
                where2[n][m] = 's';
                return 0;
            }
            else{
                cout<<endl<<"You already have a ship in this spot! "<<endl;
                cout<<"Press Enter to continue"<<endl;
                do{}
                while(cin.get() != '\n');
                return 1;
            }
        }
        
    }
    else{
        cout<<"\nPlease enter Coordinates between 0-4. "<<endl;
        cout<<"Press Enter to continue"<<endl;
        do{}
        while(cin.get() != '\n');
        return 1;
    }
    
}
*/


//checks whether all the ships on a player's board have been hit completely
char check(char player)
{
    int i, j, hits=0;
    
    if (player == '1'){
        for (i = 0; i < 5; i++) {
            for (j = 1; j < 5; j++) {
                if (where1[i][j] == 'H') {
                    hits++;
                    if(hits >= 5)
                        return 'l';
                }
            }
        }
    }
    else{
        for (i = 0; i < 5; i++) {
            for (j = 1; j < 5; j++) {
                if (where2[i][j] == 'H') {
                    hits++;
                    if(hits >= 5)
                        return 'm';
                }
            }
        }
    }
    return 'p';
}


char shipHit(char s, char player) {
    if (s == 3) {
        cout<<"You have sunk a ship!"<<endl;
    }
    return check(player);
}

//attack funciton that displays a hit or miss depending on user choice
int attack(char person, int n, int m){
    int s1 = 0;
    int s2 = 0;
    int s3 = 0;
    int s4 = 0;
    int s5 = 0;
    int s6 = 0;
    int s7 = 0;
    int s8 = 0;
    
    if(n >= 0 && n <= 4 && m >= 0 && n <= 4){
        if(person == '1'){
            if(where1[n][m] == '1'){
                cout<<"That's a HIT! "<<endl;
                where1[n][m] = 'H';
                oneShips[n][m] = 'H';
                s1++;
                shipHit(s1, '1');
                return 0;
            }
            if (where1[n][m] == '2') {
                cout<<"That's a HIT! "<<endl;
                where1[n][m] = 'H';
                oneShips[n][m] = 'H';
                s2++;
                shipHit(s2, '1');
                return 0;
            }
            if (where1[n][m] == '3') {
                cout<<"That's a HIT! "<<endl;
                where1[n][m] = 'H';
                oneShips[n][m] = 'H';
                s3++;
                shipHit(s3, '1');
                return 0;
            }
            if (where1[n][m] == '4') {
                cout<<"That's a HIT! "<<endl;
                where1[n][m] = 'H';
                oneShips[n][m] = 'H';
                s4++;
                shipHit(s4, '1');
                return 0;
            }
            if(where1[n][m] == 'H'){
                cout<<"You already hit them there and wasted a turn ;( "<<endl;
                return 0;
            }
            if(where1[n][m] == 'M'){
                cout<<"You already Missed them there and wasted a turn ;( "<<endl;
                return 0;
            }
            else{
                cout<<"Missed em "<<endl;
                where1[n][m] = 'M';
                oneShips[n][m] = 'M';
                return 0;
            }
        }
        else{
            if(where2[n][m] == '5'){
                cout<<"That's a HIT! "<<endl;
                where2[n][m] = 'H';
                twoShips[n][m] = 'H';
                s5++;
                shipHit(s5, '2');
                return 0;
            }
            if (where2[n][m] == '6') {
                cout<<"That's a HIT! "<<endl;
                where2[n][m] = 'H';
                twoShips[n][m] = 'H';
                s6++;
                shipHit(s6, '2');
                return 0;
            }
            if (where2[n][m] == '7') {
                cout<<"That's a HIT! "<<endl;
                where2[n][m] = 'H';
                twoShips[n][m] = 'H';
                s7++;
                shipHit(s7, '2');
                return 0;
            }
            if (where2[n][m] == '8') {
                cout<<"That's a HIT! "<<endl;
                where2[n][m] = 'H';
                twoShips[n][m] = 'H';
                s8++;
                shipHit(s8, '2');
                return 0;
            }
            if(where2[n][m] == 'H'){
                cout<<"You already hit them there and wasted a turn ;( "<<endl;
                return 0;
            }
            if(where2[n][m] == 'M'){
                cout<<"You already Missed them there and wasted a turn ;( "<<endl;
                return 0;
            }
            else{
                cout<<"Missed em "<<endl;
                where2[n][m] = 'M';
                twoShips[n][m] = 'M';
                return 0;
            }
        }
    }
    else{
        cout<<"\nPlease enter Coordinates between 0-4"<<endl;
        cout<<"Press Enter to continue"<<endl;
        do{}
        while(cin.get() != '\n');
        return 1;
    }
}

//place boat somewhere on your board
int placeBoat(int x, int y, char name, int dir){
    if(x >= 0 && x <= 4 && y >= 0 && y <= 4 && 0 < dir && dir < 5){
        if (name == '1'){
            switch(dir){
                case 1:
                    where1[x][y] = '1';
                    where1[x][y-1] = '1';
                    where1[x][y-2] = '1';
                    return 0;
                case 2:
                    where1[x][y] = '2';
                    where1[x+1][y] = '2';
                    where1[x+2][y] = '2';
                    return 0;
                case 3:
                    where1[x][y] = '3';
                    where1[x][y+1] = '3';
                    where1[x][y+2] = '3';
                    return 0;
                case 4:
                    where1[x][y] = '4';
                    where1[x-1][y] = '4';
                    where1[x-2][y] = '4';
            }
        }
        else{
            switch(dir){
                case 1:
                    where2[x][y] = '5';
                    where2[x][y-1] = '5';
                    where2[x][y-2] = '5';
                    return 0;
                case 2:
                    where2[x][y] = '6';
                    where2[x+1][y] = '6';
                    where2[x+2][y] = '6';
                    return 0;
                case 3:
                    where2[x][y] = '7';
                    where2[x][y+1] = '7';
                    where2[x][y+2] = '7';
                    return 0;
                case 4:
                    where2[x][y] = '8';
                    where2[x-1][y] = '8';
                    where2[x-2][y] = '8';
                    return 0;
            }
        }
    }
    else{
        cout<<"\nPlease enter Coordinates between 0-4"<<endl;
        cout<<"Press Enter to continue"<<endl;
        do{}
        while(cin.get() != '\n');
        return 1;
    }
    return 1;
}




