///Portia Seater

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <time.h>
#include <math.h>

int gcd(int a, int b) {
    while (b) {
        int c = 0;
        c = b;
        b = a%b;
        a = c;
    }
    return a;
}

void createKeys(int size) {
    int start, end = 0;
    int z = 0;
    int p = 607;
    int q = 269;
    int n = p*q;
    int d,e = 0;
    int pubKey [2] = {};
    int privKey [2] = {};
    
    while (true) {
        start = pow(2, (size-1));
        end = pow(2, size);
        e = rand() % end + start;
        z = gcd(e, (p - 1) * (q - 1));
        if (z == 1) {
            break;
        }
    }
    
    d = (p+q)/2;
    
    pubKey [0] = n;
    pubKey [1] = e;
    privKey [0] = n;
    privKey [1] = d;
    
    std::cout << "Public: " << pubKey[0] << ", " << pubKey[1] << std::endl;
    std::cout << "Private: " << privKey[0] << ", " << privKey[1] << std::endl;
}

//void saveKeys(name, size) {

//}

int main(int argc, char *argv[]) {
    srand(time(NULL));
    
    createKeys(1024);
    printf("Done.");
    
}