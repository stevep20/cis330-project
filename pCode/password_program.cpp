//Portia Seater

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>
//#include <vigenere_encryption.h>

using namespace std;

const int ALPHABET_SIZE = 26;

int getLetterNum(char letter) {
    // 'a' becomes 0, 'b' becomes 1, etc. ' ' becomes 26. Other behavior is
    // undefined but not explicitly protected against.
    if (letter == ' ') {
        letter = 'z' + 1;
    }
    return (int)letter - 'a';
}

char getCharFromNum(int num) {
    // 0 becomes 'a', 1 becomes 'b', etc. 26 becomes ' '. Other behavior is
    // undefined but not explicitly protected against.
    if (num == 26) {
        return ' ';
    }
    return (char)num + 'a';
}

std::string numericKeyToAlphabetical(std::string numericKey) {
    // Converts a string of numeric characters to the corresponding letters.
    // '0' becomes 'a', '1' becomes 'b', ... , '9' becomes 'j'. No letters
    // beyond j can be produced, because there are only 10 numerals.
    // Any non-numeric characters in numericKey are removed from the result,
    // such that "00.0" becomes "aaa".
    char result[numericKey.length()];
    int i=0;
    for (std::string::iterator it=numericKey.begin(); it!=numericKey.end(); ++it) {
        if (isdigit(*it)) {
            result[i++] = *it - '0' + 'a';
        }
    }
    result[i] = '\0';
    std::string returnVal = result;
    return result;
}

/*
 Encrypt a text with a key. If key length >= text length, it's a one time pad.
 If text length > key length > 1, it's a Vigenere cipher. If key length = 1,
 it's a Caesar shift.
 
 The only characters that are explicitly supported for keys are lowercase
 letters and spaces. If you have other characters in the key, the function
 might still work, but I didn't design it with that in mind, so there may be
 some edge cases that break the function. And anyway, since all the math is done
 using modulo 26/27, using more characters than that in your key won't make it
 any more secure.
 
 If decrypt = true, the function will decrypt a ciphertext that was previously
 encrypted with the given key.
 
 We pretend that the keytext repeats itself indefinitely, so that encrypting
 something with "hi" actually encrypts it with "hihihihihihihi....".
 
 Characters that are uppercase in the input text remain uppercase in the output
 text. (Unless you enable allLowerCase, in which case the output will be all
 lower case.)
 
 Unless stripUnsupportedChars is enabled, any characters that aren't being
 encrypted will be left unmolested in the output text. (E.g. "this.?!abcd" will
 be encrypted to "xxxx.?!xxxx", where x's represent encrypted letters.)
 */
std::string vigenereEncrypt(std::string text, std::string keytext, bool decrypt /*=false*/, bool encryptSpaces /*=false*/, bool allLowerCase/*=false*/, bool stripUnsupportedChars/*=false*/) {
    char result[text.length() + 1]; //Array to hold encrypted std::string
    result[text.length()] = '\0'; //We already know that we need to put \0 here
    
    int decryptModifier = 1;
    if (decrypt) {
        decryptModifier = -1;
        // If we're encrypting, we want to add the key to the text; if
        // decrypting, we want to subtract the key. We multiply the key values
        // by decryptModifier, so they'll be positive if we're encrypting, and
        // negative if we're decrypting.
    }
    
    int alphabetLength = ALPHABET_SIZE;
    if (encryptSpaces) {
        ++alphabetLength; // space also counts as a 'letter' in this case
    }
    
    bool wasCapital; // So the program can remember if a letter was capital
    char currentChar;
    int tempCharNumber; // will be 0 if 'a', 1 if 'b', etc
    int keyCharNumber; // same deal as tempCharNumber
    int keyIndex = 0; // for incrementing through keytext
    
    for (unsigned int i=0; i < text.length(); ++i) {
        currentChar = text[i];
        if (isalpha(currentChar) || (encryptSpaces && currentChar == ' ')) {
            // ^ If a character is one we need to encrypt
            
            wasCapital = isupper(currentChar); // Remembers if we need to
            // convert back to uppercase later
            
            tempCharNumber = getLetterNum(tolower(currentChar));
            keyCharNumber = getLetterNum(keytext[keyIndex]) * decryptModifier;
            //If we're decrypting, we want to subtract the key instead of
            //adding it, so decryptModifier will be -1. Otherwise it will be 1.
            
            tempCharNumber = (tempCharNumber + keyCharNumber) % (alphabetLength - (int)(encryptSpaces && wasCapital));
            //  - (encryptSpaces && wasCapital) because capital letters cannot
            // become spaces because the assignment demands that decrypted text
            // must EXACTLY match the text before it was encrypted. I personally
            // think that was a poor design decision.
            if (tempCharNumber < 0) {
                tempCharNumber += (alphabetLength - (encryptSpaces && wasCapital));
                // I must do this because C's % operator is dumb and can give
                // negative numbers
            }
            
            // Now we turn it back into real char, and make uppercase if needed
            currentChar = getCharFromNum(tempCharNumber);
            if (wasCapital && ! allLowerCase) {
                currentChar = toupper(currentChar);
            }
            // Now we have the encrypted letter in currentChar
            
            // Now we need to increment keyIndex. Since we want to pretend the
            // key repeats itself indefinitely, we perform addition modulo
            // (size of key), so that the index goes back to 0 after reaching
            // the end of the string:
            keyIndex = (keyIndex + 1) % keytext.length();
            result[i] = currentChar;
        } else if (! stripUnsupportedChars) {
            result[i] = currentChar;
        }
    }
    std::string returnVal = result; // Too lazy to figure out how to do this
    // without declaring a new variable
    return returnVal;
}

string getPassword() {
    string word;
    
    cout << "What is the password for your session?: " << endl;
    cin >> word;
    cout << "Entering specified chat..." << endl;
    return word;
}

int main(int argc, char *argv[]) {
    string password;
    string encrypted;
    string decrypted;
    password = getPassword();
    encrypted = vigenereEncrypt("hello bob", password, false, true, false, false);
    decrypted = vigenereEncrypt(encrypted, password, true, true, false, false);
    cout << encrypted << endl;
    cout << decrypted << endl;
}

